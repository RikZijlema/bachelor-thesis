#!/usr/bin/env python3
# Bag of words classifier used on testing set

import pandas
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def main():
    """Opening the data sets"""
    training_data = pandas.read_csv(open("training.csv"), sep=';', header=None)
    testing_data = pandas.read_csv(open("testing.csv"), sep=';', header=None)
    testing_data.columns = ["tweet","tag"]
    testing_tweets = testing_data["tweet"]
    testing_tags = testing_data["tag"]
    training_data.columns = ["tweet","tag"]
    tweets = training_data["tweet"]
    tags = training_data["tag"]
    """Initialize vectorizer"""
    tfidf_vect = TfidfVectorizer(ngram_range=(1,1), stop_words=["een","wat","is","hoe","over","mij","dit","dan","verder","alleen","andere","behalve","eerder","elke",
            "zulk"])
    X_train = tfidf_vect.fit_transform(tweets).toarray()
    X_test = tfidf_vect.transform(testing_tweets)
    y_train = tags
    y_test = testing_tags
    clf = LinearSVC().fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    """Evaluation"""
    per_class = precision_recall_fscore_support(y_test,y_pred)
    feature_names = tfidf_vect.get_feature_names()
    class_labels = ["AGA","FAV","NEU"]
    """Obtains the top 10 features per class"""
    for i, class_label in enumerate(class_labels):
        top10 = np.argsort(clf.coef_[i])[-10:]
        print("%s: %s" % (class_label,
              " ".join(feature_names[j] for j in top10)))
    print("AGA F-Score: ",per_class[2][0])
    print("FAV F-Score: ",per_class[2][1])
    print("NEU F-Score: ",per_class[2][2])

if __name__ == "__main__":
    main()
