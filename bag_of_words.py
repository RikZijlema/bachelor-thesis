#!/usr/bin/env python3
# Bag of words classifier for training

import pandas
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def main():
    """Open the dataset"""
    data = pandas.read_csv(open("training.csv"), sep=';', header=None)
    data.columns = ["tweet","tag"]
    tweets = data["tweet"]
    tags = data["tag"]
    """Initialize vectorizer"""
    tfidf_vect = TfidfVectorizer(ngram_range=(1,1), stop_words=["een","wat","is","hoe","over","mij","dit","dan","verder","alleen","andere","behalve","eerder","elke",
            "zulk"])
    X = tfidf_vect.fit_transform(tweets).toarray()
    y = tags
    cv = KFold(n_splits=10, random_state=None, shuffle = False)
    """Lists that will contain 10 values after 10-fold cross-validation"""
    accuracy_list = []
    precision_list = []
    recall_list = []
    f_score_list = []
    AGA_list = []
    FAV_list = []
    NEU_list = []
    """10-Fold cross-validation"""
    for train_index, test_index in cv.split(X):
        X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
        clf = LinearSVC().fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        """Evaluation"""
        accuracy = accuracy_score(y_test, y_pred)
        recall = recall_score(y_test,y_pred, average='macro')
        precision = precision_score(y_test,y_pred, average='macro')
        f_score = f1_score(y_test,y_pred, average='macro')
        per_class = precision_recall_fscore_support(y_test,y_pred)
        accuracy_list.append(accuracy)
        precision_list.append(precision)
        recall_list.append(recall)
        f_score_list.append(f_score)
        AGA_list.append(per_class[2][0])
        FAV_list.append(per_class[2][1])
        NEU_list.append(per_class[2][2])
    """Calculating the averages of the lists"""
    avg_accuracy = np.mean(accuracy_list)
    avg_precision = np.mean(precision_list)
    avg_recall = np.mean(recall_list)
    avg_f_score = np.mean(f_score_list)
    avg_AGA = np.mean(AGA_list) 
    avg_FAV = np.mean(FAV_list)
    avg_NEU = np.mean(NEU_list)
    """Printing the averages"""
    print("Average accuracy: ",avg_accuracy)
    print("Average precision: ",avg_precision)
    print("Average recall: ",avg_recall)
    print("Average F-Score: ",avg_f_score)
    print("Average AGA F-score: ",avg_AGA)
    print("Average FAV F-score: ",avg_FAV) 
    print("Average NEU F-score: ",avg_NEU)

if __name__ == "__main__":
    main()
