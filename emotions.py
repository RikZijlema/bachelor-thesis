#!/usr/bin/env python3
# Script used to add the emotion weights as features
import pandas

def main():
    """Open data set"""
    data = pandas.read_csv(open("emotions.csv"), sep=';', header=None)
    data.columns = ["word","anger","anticipation","disgust","fear","joy","sadness","surprise","trust"]
    tweets = pandas.read_csv(open("testing.csv"), sep=';', header=None)
    tweets.columns = ["tweet","tag"]
    tweet = tweets["tweet"]
    em_word = data["word"]
    anger_list = []
    anticipation_list = []
    disgust_list = []
    fear_list = []
    joy_list = []
    sadness_list = []
    surprise_list = []
    trust_list = []
    """Iterate over each tweet"""
    for tweet in tweet:
        anger_count = 0
        anticipation_count = 0
        disgust_count = 0
        fear_count = 0
        joy_count = 0
        sadness_count = 0
        surprise_count = 0
        trust_count = 0
        """Iterates over each word of the tweet"""
        for word in tweet.split():
            word = word.lower()
            word = word.rstrip("!.,?",)
            """Iterates over the Dutch translation of NRC lexicon"""
            for row in data.itertuples():
                emotion_word = row[1]
                anger = row[2]
                anticipation = row[3]
                disgust = row[4]
                fear = row[5]
                joy = row[6]
                sadness = row[7]
                surprise = row[8]
                trust = row[9]
                """If word in tweet corresponds to word in row"""
                if word == emotion_word:
                    if anger == "1":
                        anger_count += 1
                    elif anticipation == "1":
                        anticipation_count += 1
                    elif disgust == "1":
                        disgust_count += 1
                    elif fear == "1":
                        fear_count += 1
                    elif joy == "1":
                        joy_count += 1
                    elif sadness == "1":
                        sadness_count += 1
                    elif surprise == "1":
                        surprise_count += 1
                    elif trust == "1":
                        trust_count += 1
                    else:
                        pass
            else:
                pass
        """Calculation of the emotion weights"""
        tweet_length = len(tweet.split())
        anger_list.append((anger_count/tweet_length))
        anticipation_list.append((anticipation_count/tweet_length))
        disgust_list.append((disgust_count/tweet_length))
        fear_list.append((fear_count/tweet_length))
        joy_list.append((joy_count/tweet_length))
        sadness_list.append((sadness_count/tweet_length))
        surprise_list.append((surprise_count/tweet_length))
        trust_list.append((trust_count/tweet_length))

    """Creates a file containing the emotion weights for each tweet"""
    tweets["anger"] = anger_list
    tweets["anticipation"] = anticipation_list
    tweets["disgust"] = disgust_list
    tweets["fear"] = fear_list
    tweets["joy"] = joy_list
    tweets["sadness"] = sadness_list
    tweets["surprise"] = surprise_list
    tweets["trust"] = trust_list
    tweets.to_csv("testing_emo.csv")

if __name__ == "__main__":

    main()

