#!/usr/bin/env python3
# Majority Class Classifier for testing set

import pandas
from sklearn.metrics import precision_recall_fscore_support
from sklearn.dummy import DummyClassifier
import numpy as np

def main():
    """Opening the datasets"""
    training_data = pandas.read_csv(open("training.csv"), sep=';', header=None)
    testing_data = pandas.read_csv(open("testing.csv"), sep=';', header=None)
    testing_data.columns = ["tweet","tag"]
    testing_tweets = testing_data["tweet"]
    testing_tags = testing_data["tag"]
    training_data.columns = ["tweet","tag"]
    tweets = training_data["tweet"]
    tags = training_data["tag"]
    """Assigning X,Y values for classifier and prediction"""
    X_train = tweets
    X_test = testing_tweets
    y_train = tags
    y_test = testing_tags
    """Prediction"""
    clf = DummyClassifier(strategy="most_frequent").fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    """Calculate and print F-scores per class"""
    per_class = precision_recall_fscore_support(y_test,y_pred)
    print("AGA F-Score: ",per_class[2][0])
    print("FAV F-Score: ",per_class[2][1])
    print("NEU F-Score: ",per_class[2][2])
if __name__ == "__main__":
    main()
