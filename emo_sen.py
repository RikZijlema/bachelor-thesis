#!/usr/bin/env python3
# Emotion/sentiment classifier used for training

import pandas
from sklearn.svm import LinearSVC
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def main():
    """Opening data set"""
    data = pandas.read_csv(open("training_emosen.csv"), sep=',', header=None)
    data.columns = ["number","tweet","tag","anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]
    """Assigning tags and features"""
    tags = data["tag"]
    features = data[["anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]]
    X = np.array(features)
    y = tags
    cv = StratifiedKFold(n_splits=10, random_state=None, shuffle = False)
    accuracy_list = []
    precision_list = []
    recall_list = []
    f_score_list = []
    AGA_list = []
    FAV_list = []
    NEU_list = []
    """10-fold cross-validation"""
    for train_index, test_index in cv.split(X,y):
        X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
        clf = LinearSVC().fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        """Evaluation"""
        accuracy = accuracy_score(y_test, y_pred)
        recall = recall_score(y_test,y_pred, average='macro')
        precision = precision_score(y_test,y_pred, average='macro')
        f_score = f1_score(y_test,y_pred, average='macro')
        per_class = precision_recall_fscore_support(y_test,y_pred)
        AGA_list.append(per_class[2][0])
        FAV_list.append(per_class[2][1])
        NEU_list.append(per_class[2][2])
        accuracy_list.append(accuracy)
        precision_list.append(precision)
        recall_list.append(recall)
        f_score_list.append(f_score)
    """Calculate the average of each list"""
    avg_accuracy = np.mean(accuracy_list)
    avg_precision = np.mean(precision_list)
    avg_recall = np.mean(recall_list)
    avg_f_score = np.mean(f_score_list)
    avg_AGA = np.mean(AGA_list) 
    avg_FAV = np.mean(FAV_list)
    avg_NEU = np.mean(NEU_list)
    """Print the averages"""
    print("Average accuracy: ",avg_accuracy)
    print("Average precision: ",avg_precision)
    print("Average recall: ",avg_recall)
    print("Average F-Score: ",avg_f_score)
    print("Average AGA F-score: ",avg_AGA)
    print("Average FAV F-score: ",avg_FAV) 
    print("Average NEU F-score: ",avg_NEU)

if __name__ == "__main__":
    main()
