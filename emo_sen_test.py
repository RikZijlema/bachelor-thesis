#!/usr/bin/env python3
# Emotion/sentiment classifier used on testing set

import pandas
from sklearn.svm import LinearSVC
from sklearn.metrics import precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def main():
    """Open the datasets"""
    training_data = pandas.read_csv(open("training_emosen.csv"), sep=',', header=None)
    testing_data = pandas.read_csv(open("testing_emosen.csv"), sep=',', header=None)
    testing_data.columns = ["number","tweet","tag","anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]
    training_data.columns = ["number","tweet","tag","anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]
    """Assigning tags and features"""
    training_tags = training_data["tag"]
    training_features = training_data[["anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]]
    testing_features = testing_data[["anger","anticipation","disgust",
                    "fear","joy","sadness","surprise","trust","sentiment"]]
    testing_tags = testing_data["tag"]
    X_train = np.array(training_features)
    y_train = training_tags
    X_test = np.array(testing_features)
    y_test = testing_tags
    clf = LinearSVC().fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    """Evaluation"""
    per_class = precision_recall_fscore_support(y_test,y_pred)
    print("Average AGA F-score: ",per_class[2][0])
    print("Average FAV F-score: ",per_class[2][1]) 
    print("Average NEU F-score: ",per_class[2][2])

if __name__ == "__main__":
    main()
