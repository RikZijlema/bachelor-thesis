#!/usr/bin/env python3
# Script used to calculate inter-annotator agreement
import pandas
from sklearn.metrics import cohen_kappa_score

def main():
    data = pandas.read_csv(open("IAA.csv"), sep=';', header=None)
    data.columns = ["ann1","ann2"]
    ann1 = data["ann1"]
    ann2 = data["ann2"]
    print(cohen_kappa_score(ann1,ann2))
if __name__ == "__main__":
    main()
