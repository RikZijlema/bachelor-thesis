#!/usr/bin/env python3
# Script used to add sentiment feature

import pandas
from pattern.nl import sentiment

def main():
    data = pandas.read_csv(open("testing.csv"), sep=';', header=None)
    data.columns = ["tweet","tag"]
    tweets = data["tweet"]
    tags = data["tag"]
    sentiments = []
    for tweet in tweets:
        sentiments.append(sentiment(tweet)[0])
    data["sentiment"] = sentiments
    data.to_csv("testing_em.csv")
if __name__ == "__main__":

    main()

