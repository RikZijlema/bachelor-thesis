#!/usr/bin/env python3
# Script used to count the amount of tweets considered positive,
# negative and neutral

def main():
    file = open("polarity_testing.txt","r")
    neg_count = 0
    pos_count = 0
    neu_count = 0
    for line in file:
        line = line.rstrip("\n")
        line = float(line)
        if line < 0:
            neg_count += 1
        if line > 0:
            pos_count += 1
        if line == 0.0:
            neu_count += 1
    print(neg_count)
    print(pos_count)
    print(neu_count)
    print(neg_count + pos_count + neu_count)
if __name__ == "__main__":
    main()
